<?php

function image_mattach_edit_form($form_state, $node) {
  $form_state['storage']['nid'] = $node->nid;
  $images = _image_mattach_get_attached($node->nid);

  $form['attached'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attached images')
  );
  $rows = array(
    '#tree' => TRUE,
    '#theme' => 'image_mattach_list'
  );
  $rows['images'] = array(
    '#type' => 'value',
    '#value' => $images,
  );
  foreach ($images as $id => $image) {
    $rows[$id]['delete'] = array('#type' => 'checkbox');
    $rows[$id]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $image->weight,
    );
  }
  $form['attached']['del'] = $rows;
  if (count($images)) {
    $form['attached']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
      '#submit' => array('image_mattach_edit_delete')
    );
  }
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('All images')
  );
  $all = _image_mattach_get_image_nodes();
  $rows = array(
    '#tree' => TRUE,
    '#theme' => 'image_mattach_browse'
  );
  $rows['images'] = array(
    '#type' => 'value',
    '#value' => $all,
  );
  foreach ($all as $id => $image) {
    $rows[$id]['add'] = array('#type' => 'checkbox', '#disabled' => isset($images[$id]));
  }
  $form['list']['new'] = $rows;
  $form['list']['pager'] = array(
    '#type' => 'markup',
    '#value' => theme('pager', NULL, variable_get('default_nodes_main', 10))
  );
  if (count($all)) {
    $form['list']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('image_mattach_edit_add')
    );
  }
  return $form;
}

function image_mattach_edit_add($form, &$form_state) {
  global $user;
  $nid = $form_state['storage']['nid'];
  $rows = $form_state['values']['new'];
  $images = $rows['images'];
  foreach ($images as $k => $v) {
    if ($rows[$k]['add']) {
  	  db_query("INSERT INTO {image_mattach} (nid, iid, uid) VALUES (%d, %d, %d)", $nid, $k, $user->uid);
    }
  }
}

function image_mattach_edit_delete($form, &$form_state) {
  global $user;
  $nid = $form_state['storage']['nid'];
  $rows = $form_state['values']['del'];
  $images = $rows['images'];

  db_query("DELETE FROM {image_mattach} WHERE nid = %d", $nid);
  foreach ($images as $k => $v) {
    if (!$rows[$k]['delete']) {
  	  db_query("INSERT INTO {image_mattach} (nid, iid, uid, weight) VALUES (%d, %d, %d, %d)", $nid, $k, $user->uid, $rows[$k]['weight']);
    }
  }
}

/**
 * Fetch an array of all candidate referenced nodes, for use in presenting the selection form to the user.
 */
function _image_mattach_get_image_nodes() {
  $result = pager_query(db_rewrite_sql("SELECT n.nid, n.title, f.filepath FROM {node} n LEFT JOIN {image} i ON i.nid = n.nid LEFT JOIN {files} f ON f.fid = i.fid WHERE n.type='image' AND n.status = 1 AND i.image_size = 'thumbnail' ORDER BY n.sticky DESC, n.created DESC, n.title ASC"), variable_get('default_nodes_main', 10));
  //$result_count = db_result(db_query("SELECT COUNT(*) FROM {node} n WHERE n.status=1 AND type='image'"));
  $rows = array();
  while ($node = db_fetch_object($result)) {
    $rows[$node->nid] = $node;
  }
  return $rows;
}

function _image_mattach_get_attached($nid) {
  $result = db_query("SELECT n.nid, n.title, f.filepath, im.weight FROM {image_mattach} im LEFT JOIN {node} n ON n.nid = im.iid LEFT JOIN {image} i ON i.nid = n.nid LEFT JOIN {files} f ON f.fid = i.fid WHERE im.nid = %d AND im.status=1 AND i.image_size = 'thumbnail' ORDER BY im.weight, n.title", $nid);
  $rows = array();
  while ($node = db_fetch_object($result)) {
    $rows[$node->nid] = $node;
  }
  return $rows;
}

/**
 * Theme node images list.
 */
function theme_image_mattach_list($form) {
  $header = array(t('Delete'), t('Weight'), t('Title'), t('Thumbnail'));
  drupal_add_tabledrag('attached-images', 'order', 'sibling', 'attached-images-weight');

  $rows = array();
  foreach ($form['images']['#value'] as $id => $image) {
    $form[$id]['weight']['#attributes']['class'] = 'attached-images-weight';

    $row = array();
    $row[] = drupal_render($form[$id]['delete']);
    $row[] = drupal_render($form[$id]['weight']);
    $row[] = check_plain($image->title);
    $row[] = theme('image', $image->filepath, $image->title, $image->title, array('vspace' => '3'));

    $rows[] = array('data' => $row, 'class' => 'draggable');
  }

  $output = theme('table', $header, $rows, array('id' => 'attached-images'));
  $output .= drupal_render($form);

  return $output;
}

/**
 * Theme node images list.
 */
function theme_image_mattach_browse($form) {
  $header = array(t('Add'), t('Title'), t('Thumbnail'));

  $rows = array();
  foreach ($form['images']['#value'] as $id => $image) {

    $row = array();
    $row[] = drupal_render($form[$id]['add']);
    $row[] = check_plain($image->title);
    $row[] = theme('image', $image->filepath, $image->title, $image->title, array('vspace' => '3'));

    $rows[] = array('data' => $row, 'class' => ($form[$id]['add']['#disabled']) ? 'disabled' : '');
  }

  $output = theme('table', $header, $rows, array('id' => 'attach-all-images'));
  $output .= drupal_render($form);

  return $output;
}
