<?php

/**
 * Implementation of hook_views_data().
 */
function image_mattach_views_data() {
  // Basic table information.
  $data = array();

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['image_mattach']['table']['group']  = t('Image mattach');

  // For other base tables, explain how we join
  // LEFT is the default, but let's be explicit
  $data['image_mattach']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      'type' => 'LEFT',
    ),
  );

  // changed field
  $data['image_mattach']['iid'] = array(
    'title' => t('MAttached image'), // The item it appears as on the UI,
    'field' => array(
      'handler' => 'image_mattach_views_handler_field_iid',
      'click sortable' => TRUE,
    ),
    'help' => t('An image mattached to a node.'),
  );

  return $data;
}
