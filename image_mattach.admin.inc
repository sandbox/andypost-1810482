<?php

function image_mattach_admin_settings() {
  $form = array();

  $form['image_mattach_max_images'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of images'),
    '#default_value' => variable_get('image_mattach_max_images', 4),
    '#size' => 6,
    '#maxlength' => 2,
    '#description' => t('The maximum number of images a user can attach for each node.'),
  );

  return system_settings_form($form);
}
