<?php
// $Id$

/**
 * @file image_mattach.module
 */

define('IMAGE_MATTACH_HIDDEN', 'hidden');

/**
 * Implementation of hook_help().
 */
function image_mattach_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/modules#description':
      return t('Allows easy attaching of image nodes to other content types.');
  }
}

/**
 * Implementation of hook_perm().
 */
function image_mattach_perm() {
  return array('attach images', 'delete terms');
}

/**
 * Implementation of hook_menu()
 */
function image_mattach_menu() {
  $items = array();

  $items['admin/settings/image_mattach'] = array(
    'title' => 'Image attach',
    'description' => 'Enable image attach for content',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('image_mattach_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'image_mattach.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['node/%node/mimages'] = array(
    'title' => 'Images',
    'page callback' => 'drupal_get_form',
    //'page callback' => 'image_mattach_edit_form',
    'page arguments' => array('image_mattach_edit_form', 1),
    'access callback' => 'image_mattach_subaccess',
    'access arguments' => array(1, 'images'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'image_mattach.edit.inc',
  );

  return $items;
}

/**
 * Menu item access callback - determine if the images tab is accessible.
 */
function image_mattach_subaccess($node, $op) {
  $result = (variable_get('image_mattach_'. $node->type, 0) != 0);
  if ($result) {
    if ($op == 'gallery') {
      return node_access('view', $node);
    }

    if ($op == 'images') {
      global $user;
      $may_add = array_intersect(array_keys($user->roles), variable_get('image_mattach_roles_add_'. $node->type, array()));
      return user_access('attach images') && (node_access('update', $node) || !empty($may_add));
    }
  }
  return false;
}

/**
 * Implementation of hook_block().
 */
/*
function image_mattach_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0] = array(
        'info'       => t('Attached Images'),
        'status'     => TRUE,
        'weight'     => 0,
        'visibility' => 1,
        'pages'      => 'node/*',
      );
      return $blocks;

    case 'view':
      if ($delta == 0) {
        if (arg(0) == 'node' && is_numeric(arg(1))) {
          $node = node_load(arg(1));
          if (isset($node->iid) && $node->iid) {
            $image = node_load($node->iid);
            if (node_access('view', $image)) {
              $img = image_display($image, variable_get('image_mattach_block_0_size', IMAGE_THUMBNAIL));
              return array(
                'subject' => t('Attached Images'),
                'content' => l($img, "node/$node->iid", array('html' => TRUE)),
              );
            }
          }
        }
      }
      break;

    case 'configure':
      if ($delta == 0) {
        $image_sizes = array();
        foreach (image_get_sizes() as $key => $size) {
          $image_sizes[$key] = $size['label'];
        }
        $form['image_mattach_block_0_size'] = array(
          '#type' => 'select',
          '#title' => t('Image size'),
          '#default_value' => variable_get('image_mattach_block_0_size', IMAGE_THUMBNAIL),
          '#options' => $image_sizes,
          '#description' => t("This determines the size of the image that appears in the block."),
        );
        return $form;
      }
      break;

    case 'save':
      if ($delta == 0) {
        variable_set('image_mattach_block_0_size', $edit['image_mattach_block_0_size']);
      }
      break;
  }
}
*/

/**
 * implementation of hook_form_alter()
 */
function image_mattach_form_alter(&$form, $form_state, $form_id) {
  // Content type settings form.
  if ($form_id == 'node_type_form' && $form['#node_type']->type != 'image') {
    _image_check_settings();
    $type = $form['#node_type']->type;

    $image_sizes = array(IMAGE_MATTACH_HIDDEN => t('<Hidden>'));
    foreach (image_get_sizes() as $key => $size) {
      $image_sizes[$key] = $size['label'];
    }

    $form['image_mattach'] = array(
      '#type' => 'fieldset',
      '#title' => t('Image Multi Attach settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['image_mattach']['image_mattach'] = array(
      '#type' => 'radios',
      '#title' => t('Attach Images'),
      '#default_value' => variable_get('image_mattach_'. $type, 0),
      '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
      '#description' => t('Should this node allows users to attach an images?'),
    );
    $form['image_mattach']['image_mattach_size_teaser'] = array(
      '#type' => 'select',
      '#title' => t('Teaser image size'),
      '#default_value' => variable_get('image_mattach_size_teaser_'. $type, IMAGE_THUMBNAIL),
      '#options' => $image_sizes,
      '#description' => t("This determines the size of the image that appears when the node is displayed as a teaser. 'Hidden' will not show the image.")
    );
    $form['image_mattach']['image_mattach_weight_teaser'] = array(
      '#type' => 'weight',
      '#title' => t('Teaser image weight'),
      '#default_value' => variable_get('image_mattach_weight_teaser_'. $type, 0),
      '#description' => t("This value determines the order of the image when displayed in the teaser."),
    );
    $form['image_mattach']['image_mattach_teaser_images'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of images in node teaser'),
      '#default_value' => variable_get('image_mattach_teaser_images_'. $type, 1),
      '#size' => 5,
      '#maxlength' => 2,
      '#description' => t('The maximum number of images to show in the node teaser. 0 will not show images. Leave blank to show all images.'),
    );
    $form['image_mattach']['image_mattach_size_body'] = array(
      '#type' => 'select',
      '#title' => t('Full node image size'),
      '#default_value' => variable_get('image_mattach_size_body_'. $type, IMAGE_THUMBNAIL),
      '#options' => $image_sizes,
      '#description' => t("This determines the size of the image that appears when the full node is displayed. 'Hidden' will not show the image.")
    );
    $form['image_mattach']['image_mattach_weight_body'] = array(
      '#type' => 'weight',
      '#title' => t('Full node image weight'),
      '#default_value' => variable_get('image_mattach_weight_body_'. $type, 0),
      '#description' => t("This value determines the order of the image when displayed in the body."),
    );
    $form['image_mattach']['image_mattach_body_images'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of images in node body'),
      '#size' => 5,
      '#maxlength' => 2,
      '#default_value' => variable_get('image_mattach_body_images_'. $type, NULL),
      '#description' => t('The maximum number of images to show in the node body. 0 will not show images. Leave blank to show all images.'),
    );
  }
  // Node edit form. Make teaser always visible and no split checkbox
  if ($form_id == 'search_theme_form') {
    //$form['search_block_form']['#title'] = 'test';
    //$form['submit']['#type'] = 'image_button';
    //$form['submit']['#src'] = path_to_theme('rzc') .'/images/btn_search.gif';
    $form['search_theme_form']['#title'] = '';
    $form['submit']['#type'] = 'image_button';
    $form['submit']['#src'] = path_to_theme('rzc') .'/images/btn_search.gif';
    //$form['body_field']['teaser_include']['#type'] = 'value';
    //$form['body_field']['teaser_include']['#disabled'] = TRUE;
    //$form['body_field']['teaser_include']['#value'] = 0;
    //$form['body_field']['teaser_include']['#dafault_value'] = 0;
    //$form['body_field']['teaser_js']['#required'] = $form['type']['#value'] != 'image';
    //$form['body_field']['teaser_js']['#disabled'] = $form['type']['#value'] == 'image';
  }
  if (isset($form['#type']) && isset($form['#node']) && ($form['#node']->type == 'story')) {
    //unset($form['body_field']['teaser_include']);
  }
  if ($form_id == 'taxonomy_form_term') {
    if (!user_access('delete term')) {
      unset($form['delete']);
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function image_mattach_nodeapi(&$node, $op, $teaser, $page) {
  // Make sure that if an image is deleted it is detached from any nodes.
  if ($node->type == 'image') {
    switch ($op) {
      case 'delete':
        db_query("DELETE FROM {image_mattach} WHERE iid=%d", $node->nid);
    }
    return;
  }
  switch ($op) {

    case 'delete':
      db_query("DELETE FROM {image_mattach} WHERE nid=%d", $node->nid);
      break;

    // Pass the body and teaser objects to the theme again to add the images
    case 'view':
      $output = theme_image_mattach($node->nid, $node->type, $teaser);
      if (!empty($output)) {
        $teaser_or_body = $teaser ? 'teaser' : 'body';
        $node->content['image_mattach'] = array(
          '#value' => $output,
          '#weight' => variable_get("image_mattach_weight_{$teaser_or_body}_{$node->type}", 0),
        );
      }
      if ($node->type == 'gallery') {
        if ($teaser) {
          $cnt = db_result(db_query("SELECT count(iid) FROM {image_mattach} WHERE nid=%d", $node->nid));
          $node->title .= ' - '. format_plural($cnt, '1 photo', '@count photos');
        }
        else {
          $node->content['image_gallery'] = array(
            '#value' => '<div class="read-more">'. l(t('Photo gallery'), 'photo-gallery') .'</div>',
            '#weight' => 10,
          );
        }
      }
      break;

    case 'rss item':
      $ret = array();
      $iid = db_result(db_query_range("SELECT iid FROM {image_mattach} WHERE nid=%d ORDER by weight", $node->nid, 0, 1));
      if ($iid && $image = node_load($iid)) {
        $info = image_get_info(file_create_path($image->images[IMAGE_PREVIEW]));
        $ret[] = array(
          'key' => 'enclosure',
          'attributes' => array(
            'url' => url("node/{$image->nid}/". IMAGE_PREVIEW, array('absolute' => TRUE)),
            'length' => $info['file_size'],
            'type' => $info['mime_type'],
          )
        );
      }
      return $ret;
  }
}

/**
 * Implementation of hook_theme() registry.
 **/
function image_mattach_theme() {
  return array(
    'image_mattach' => array(
      'arguments' => array('nid' => NULL, $type => NULL, $teaser => NULL),
    ),
    'image_mattach_list' => array(
      'arguments' => array('form' => NULL),
    ),
    'image_mattach_browse' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Theme the teaser.
 *
 * Override this in template.php to include a case statement if you want different node types to appear differently.
 * If you have additional image sizes you defined in image.module, you can use them by theming this function as well.
 */
function theme_image_mattach($nid, $type, $teaser) {
  $teaser_or_body = $teaser ? 'teaser' : 'body';
  $img_size = variable_get("image_mattach_size_{$teaser_or_body}_". $type, IMAGE_THUMBNAIL);
  if ($img_size != IMAGE_MATTACH_HIDDEN) {
    if ($teaser) {
      $cnt = 0;
    }
    else {
      $cnt = NULL;
    }
    $cnt = variable_get("image_mattach_{$teaser_or_body}_images_". $type, $cnt);
    if (isset($cnt) && $cnt === 0) {
      return NULL;
    }
    if (empty($cnt)) {
      $result = db_query("SELECT n.nid, n.title, n.uid, n.status, i.image_size, f.filepath FROM {image_mattach} im LEFT JOIN {image} i ON i.nid = im.iid LEFT JOIN {files} f ON f.fid = i.fid LEFT JOIN {node} n ON n.nid = i.nid WHERE im.nid = %d AND i.image_size='%s' ORDER by weight", $nid, $img_size);
    }
    else {
      $result = db_query_range("SELECT n.nid, n.title, n.uid, n.status, i.image_size, f.filepath FROM {image_mattach} im LEFT JOIN {image} i ON i.nid = im.iid LEFT JOIN {files} f ON f.fid = i.fid LEFT JOIN {node} n ON n.nid = i.nid WHERE im.nid = %d AND i.image_size='%s' ORDER by weight", $nid, $img_size, 0, $cnt);
    }
    $mimages = array();
    while ($r = db_fetch_object($result)) {
      $r->images[$img_size] = $r->filepath;
      $r->type = 'image';
      $mimages[$r->nid] = $r;
    }

    if (count($mimages)) {

      $output = '';
      foreach ($mimages as $id => $node) {
        if (node_access('view', $node)) {
          $info = image_get_info(file_create_path($node->images[$img_size]));
          $output .= '<div style="width: '. $info['width'] .'px" class="image-attach-'. "{$teaser_or_body}" .'">';
          if ($teaser) {
            $output .= l(image_display($node, $img_size), "node/$nid", array('html' => TRUE));
          }
          else {
            $output .= l(image_display($node, $img_size), "node/$node->nid", array('html' => TRUE));
          }
          $output .= '</div>'."\n";
        }
      }
      if (!empty($output)) {
        drupal_add_css(drupal_get_path('module', 'image_mattach') .'/image_mattach.css');
        return '<div class="images">'. $output .'</div>';
      }
    }
  }
}

/**
 * Replace links for our vocabulary
 *
 * @param taxonomy_term $term
*/
function image_mattach_term_path($term) {
  return 'mission/'. $term->tid;
}

/**
 * Views 2 API integration.
 */
function image_mattach_views_api() {
  return array(
   'api' => 2,
    'path' => drupal_get_path('module', 'image_mattach'),
  );
}

/**
 * Views handlers definition.
 */
function image_mattach_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'image_mattach'),
    ),
    'handlers' => array(
      'image_mattach_views_handler_field_iid' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

